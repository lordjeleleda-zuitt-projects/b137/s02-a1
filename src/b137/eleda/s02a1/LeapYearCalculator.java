package b137.eleda.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main (String[] args) {
        System.out.println("Leap Year Calculator\n");

//        Scanner appScanner = new Scanner(System.in);
//
//        System.out.println("What is your firstname?\n");
//        String firstName = appScanner.nextLine();
//        System.out.println("Hello, " + firstName + "!\n");

        //Activity: Create a program that check if a year is a leap year or not.
        // 1900 != leap year
        // 1999 != leap year
        // 2000 = leap year
        // 2004 = leap year
        // 2012 = leap year

        int leapYear;
        Scanner leapYearCalculator = new Scanner(System.in);

        System.out.println("Is this a leap year?\n");
        leapYear = leapYearCalculator.nextInt();

        if((leapYear % 4 == 0) && (leapYear % 100 != 0)){
            System.out.println(leapYear + " is a leap year!\n");
        }
        else if ((leapYear % 4 == 0) && (leapYear % 100 == 0) && (leapYear % 400 == 0)){
            System.out.println(leapYear + " is a leap year!\n");
        }
        else {
            System.out.println(leapYear + " is not a leap year!\n");
        }

    }
}
